
#ifndef GA_H_INCLUDED
#define GA_H_INCLUDED


#include "flow.h"
#include "graph.h"
#include <vector>

//#define POPSIZE 150
//#define PSRATE 0.8
//#define PMRATE 0.005
//#define MAXGENES 5

class LinkGraph;
class MinCostFlow;

struct GeneType// a member of population
{
    int minFlowCost;//////////////////////////////最小流费用
    int buildTotalCost;
    int fitness;
    int serverNumRand;//服务器个数，初始是随机产生
    std::vector<int> serverLocateId;//服务器放置位置节点，随机产生
    std::vector<int> gene;
};


//GA
class GeneAlg
{
public:
    GeneAlg(LinkGraph* _graph ,MinCostFlow* flow,int popsize,int maxgene,double psrate,double pmrate,char *filename);
    ~GeneAlg();

    int POPSIZE;//种群规模
    int MAXGENES;//最大迭代数
    double PSRATE;//交叉概率
    double PMRATE;//变异概率
    char *savefile;

    int generation;//current generation number
    int curBest;//best individual
    int geneLength;//网络节点的个数
    int costNodeNum;//消费节点的个数
    int eachCost;/////////////////////////////服务器部署费
    int MAXCOST;
    std::vector<int> costCommon;//与消费节点相连的网络节点编号
    MinCostFlow* minCostCalculator;
    std::vector<double> cweight;//节点的权重
    double csweight;//节点的累加权重
    int bestCost;
    bool stopped;

    void Initialize();
    void Evaluate(std::vector<int> serverid,int servernum,int &mincost,int &buildcost,int &fitness);
    //void CreatOrigPop();
    void KeepBest();
    void Elitist();
    void Select();
    void Crossover();
    void Xover(int one,int two);
    void Mutate();
    void writeFile(char* filename);
    void starGA();
    void SetSwitch(bool OnOrOff);           //模拟开关

    void costCommonAsInitial(std::vector<int>& serverID,int& serverNum);
    void CommonWeightAsInitial(std::vector<int>& serverID,int& serverNum);
    void SAGoodIndividualsAsInitial(std::vector<std::vector<int> >& goodIndividuals,int& popId,std::vector<int> serverid,int servernum);

    struct GeneType bestChromosome;
    std::vector<struct GeneType> population;
    std::vector<struct GeneType> newPopulation;

    //struct GeneType population[POPSIZE+1];
    //struct GeneType newPopulation[POPSIZE+1];

private:
    /**< generate [lower, upper] random number
     */
    bool generateRandVector(std::vector<int>& randVector, int lower, int upper, int randNum);

    LinkGraph* graph;

};
#endif // GA_H_INCLUDED



