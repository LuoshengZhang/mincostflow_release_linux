#ifndef __FLOW_H__
#define __FLOW_H__

#include<vector>

const int INFI = 2147483640;

class LinkGraph;
#include <cstdlib>
//minimum cost flow declaration code here
class MinCostFlow
{

public:
    MinCostFlow(LinkGraph* _graph);

/**<  return 0 if server selection is not suitable*/
    int getCost();

    int getFinalCost();

    void saveResultToFile(char* filename);

private:
    void reset();

    bool findAugmentingPath(int s, int t);

    void rankPath();
    void findSpecificPath(int edge, size_t& pathIdx, size_t& posIdx);
    void outputPath(int pathID);
    void putIntToCharVector(std::vector<char>& outputStr, int& intNum, char* buf);

    //up and down combination
    void combineUpAndDownFlow();
    int findDownFlow(int from, int to, size_t& pathIdx, size_t&posIdx);
    bool checkPathContinue(int pathID);

 private:
    LinkGraph* graph;
    int nodeNum;//all graph node number , include common node, cost node, source, target
    int edgeNum;//all graph directed edge number ,

    std::vector<int> costDist;//used to find augmented path
    std::vector<int> prevEdge;//record one augmented path
    std::vector<int> nodeVis;//prevent from checking the front node

    std::vector< std::vector<int> > allAugmentedPaths;//the same size as allFlows
    std::vector< int > allFlows;

 private://frequently used valuables
    int costSum;
    int flow;
    int allFlow;
};



#endif
