#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <cstdlib>
#include <vector>

struct Edge
{
public:
    Edge()
    {
        from = 0;
        to = 0;
        cost = 0;
        next = -1;

        originCapacity = 0;
        updatedCapacity = 0;

        idx = 0;

        bw = 0;
        isCutted = 0;
        isVisit = 0;
    }

    int idx;
    int from;//from->to,int: 4 Bytes
    int to;
    int updatedCapacity;//used to calculate augmenting path during min cost
    int cost;//cost per bandwidth
    int next;//next circular edge id

    int originCapacity;//in order to restore updated Capacity value

    void print();

    int bw;//bandwidth
    int isCutted;
    int isVisit;
};

struct Node
{
public:
    Node()
    {
        edgeHead = -1;
        degree = 0;
        bandWidth = 0;
        inputCapacity = 0;

        degreeRatio = 0.0;
        bandWidthRatio = 0.0;
        isInner = 1.0;
    }

    double calculateWeight(double weightOfDegree = 0.5,
                           double weightOfBandWidth = 0.5,
                           double weightOfInnerNode = 0.0);
public:
    int edgeHead;//point to first outgoing edge
    double weight;//

    double degreeRatio;//[0,1]
    double bandWidthRatio;//[0,1]
    double isInner;//{0,1}

    int degree;
    int bandWidth;

    int inputCapacity;

    std::vector<int> adjacents;

//graph cutting
    std::vector<int> adjacentNodes;
    std::vector<int> adjacentEdges;

};

class LinkGraph
{
public:
    LinkGraph(){}
    ~LinkGraph();

    void init(int _commonNodeNum, int _commonNodeEdgeNum, int _costNodeNum);
    void addEdge(int _from, int _to, int _capacity, int _cost);

    void reconstructSuperSource(std::vector<int>& _serverVec, int _length);

    /**< sort common node according to weight of each node */
    void sortCostNodeAccordingDemand(double weightOfDegree = 0.7,
                                     double weightOfBandWidth = 0.25,
                                     double weightOfInnerNode = 0.05);

    void restoreEdgeCapacity();

    /**< print some key variables info */
    void printKeyGraphInfo();

    /**<  update node weight and so on*/
    void updateNodeProperty();

    /**< find the mandatory Nodes and impossible nodes */
    void preProcessing();

private:
    void quickSort(double *weightVec,int l,int r);

    /**< find the mandatory Nodes and impossible nodes */
    void updateAdjacentsForEachNode();//update adjacentNodes and adjacentEdges variables

    void branchCutting();//graph branch cutting
    void findUnipath(const int& _pathStart, std::vector<int>& _path,
                     bool setCutting = false,
                     bool isCrossBound = false);

    void initNodeTypes();

    void findMandatoryNodes();
    int getValidEdgesForEachNode(int node, std::vector<int>& _validEdges);
    void cancelEdgeVisitState(std::vector<int>& _path);
    void getFromShortPath(int& costNodeIdx, std::vector<int>& _path);
    void getFromLongPath(int& costNodeIdx, std::vector<int>& _path);
    void getFromLongPathBetweenCostNode(int& costNodeIdx, std::vector<int>& _path);

public:
    int commonNodeNum;
    int costNodeNum;
    int commonNodeEdgeNum;//just link number between common nodes, exclusion of link between costNode or super node(s, t)
    int eachServerCost;

    std::vector< int > sortedCommonNodes;//sorted node according to its weight value

    std::vector< std::pair<int,int> > costNodePairs;//[common node -> cost node] pair
    std::vector<int> sources;//save current common node id regarded as sources
    std::vector< int > costDemands;//cost command of each cost node, responding to pairs order
    int flowSum;//flow goal of super target,sum of elements in costDemands
    std::vector<int> boundNodes;
    std::vector<int> costNodes;

    std::vector<Edge> edgeVec;
    int curEdgeNum;//include all edges of the network
    std::vector<Node> nodeVec;//common node + cost node + super source + super target

    int sourceIdx;//sourceIdx = nodeVec.size() - 2
    int targetIdx;//targetIdx = nodeVec.size() - 1
    int sourceEdgeStartIdx;//index of edgeVec, used to reconstruct super source

    /**< find the mandatory Nodes and impossible nodes */
    std::vector<int> impossibleNodes;//c class
    std::vector<int> impossibleNodesSelected;

    std::vector<int> mandatoryNodes;//a class
    std::vector<int> mandatoryNodesSelected;

    std::vector<int> passNodes;
    std::vector<int> passNodesSelected;

    std::vector<int> nodeTypes;//0:inner node, 1:bound node, 2:cost node, 3: source 4: target
};





#endif
