#ifndef __ROUTE_H__
#define __ROUTE_H__

#include "lib_io.h"

class LinkGraph;
class MinCostFlow;

void deploy_server(char * graph[MAX_EDGE_NUM], int edge_num, char * filename);

//create link graph
void graphCreating(LinkGraph* graph, char * topo[MAX_EDGE_NUM], int line_num);

//genetic algorithm interface
void geneticAlgorithmEntrance(LinkGraph* graph, MinCostFlow* flow, char* filename);

//Simulated Annealing interface
void SimulatedAnnealingEntrance(LinkGraph* graph, MinCostFlow* flow, char* filename);

#endif
