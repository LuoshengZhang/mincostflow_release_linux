#ifndef __SIMULATEDANNEALING_H__
#define __SIMULATEDANNEALING_H__

#include <cstdlib>
#include <vector>
#include <string>
#include "flow.h"
//SimulatedAnnealing declaration code here

class LinkGraph;

struct SAFitness/// a member of SimulatedAnnealing fitness
{
    int inLoop;
    int outLoop;
    int upperLimit;

    double initTemp;//initial temperature
    double finalTemp;//final temperature
    float deltaTemp;//temperature decrement

    int sum;
    int addNum;
    int WeightedNum;

    bool isTaboo;
    size_t tabooNum;
};

struct ServerGroup/// a member of ServerGroup
{
    ServerGroup()
    {
        cost = INFI;
    }

    std::vector<int> serverVec;
    int  cost;

    void print();
};

class SimulatedAnnealing
{
public:
    SimulatedAnnealing(){}
    SimulatedAnnealing(LinkGraph* _graph, MinCostFlow* flow, SAFitness _fitness);
    ~SimulatedAnnealing();

    void GetServerGroup(char* filename);    //获取模拟退火求得的最优服务器数组
    void Simulation();                      //模拟过程入口
    void SetSwitch(bool OnOrOff);           //模拟开关

private:
    void InitMandatoryServerVec();  //选出必须安装服务器的网络节点
    void InitServerGroup();     //初始化服务器数组（从边界节点中减去任意一个节点的n种组合）
    void GetInitGroup();        //获得初始化服务器数组
    void UpdateGroup();         //更新服务器组（增加/减少/重置）

    void AddServerGroup();      //增加服务器（从所有普通节点中选择）
    void AddWeightedServerGroup();  //增加服务器(从weighted较高的普通节点中选择)
    void SubServerGroup();      //减少服务器

//    void SwapServerGroup();      //交换服务器（从所有普通节点中选择）
    void report();

    bool generateRandVector(std::vector<int>& randVector, int lower, int upper, int randNum);   //生成随机数

//    void replaceByAdjacentNode();
    void subPersonalNode();
    void updateVecSelected(std::vector<int>& _vec, std::vector<int>& _selected);
    bool tabooSearch(size_t layer, ServerGroup& curNewGroup);

private:
    SAFitness fitness;      //模拟退火参数表
    LinkGraph* graph;
    MinCostFlow* minCostCalculator;

    std::vector<ServerGroup> InitServerVecs; //初始服务器组合

    std::vector<int> candidateNodes;         //边界节点数组
    std::vector<int> tmpCandidateNodes;
    std::vector<int> addCandidates;
    std::vector<int> subCandidates;
    std::vector<int>::iterator vecIter;

    ServerGroup curGroup;   //当前服务器组合
    ServerGroup newGroup;   //最新服务器组合

    int mostServerNum;      //服务器最大可布置数量
    bool stopped;           //判断是否停止函数

    std::vector<int> newGroupSelected;//用于表示newGroup中哪一个节点被选择

public:
    ServerGroup bestGroup;  //较优服务器组合
    ServerGroup topGroup;  //最优服务器组合
};


#endif
