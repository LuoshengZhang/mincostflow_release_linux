#include <iostream>
#include "graph.h"
#include "flow.h"
#include <queue>
#include <cstring>
#include <cstdio>
#include "lib_io.h"

MinCostFlow::MinCostFlow(LinkGraph* _graph)
{
    graph = _graph;

    nodeNum = graph->nodeVec.size();
    edgeNum = graph->curEdgeNum;
    costDist.resize(nodeNum);
    prevEdge.resize(nodeNum);
    nodeVis.resize(nodeNum);

    reset();
}

int MinCostFlow::getCost()
{
    reset();
//    allAugmentedPaths.clear();
//    allFlows.clear();

    while(findAugmentingPath(graph->sourceIdx, graph->targetIdx))
    {
        flow = INFI;
//        std::vector<int> onePath;

        for(int ei = prevEdge[graph->targetIdx]; ei != -1; ei = prevEdge[graph->edgeVec[ei].from])
        {
            flow = flow > graph->edgeVec[ei].updatedCapacity ? graph->edgeVec[ei].updatedCapacity : flow;

//            onePath.push_back(ei);
        }
        allFlow += flow;

//        allAugmentedPaths.push_back(onePath);
//        allFlows.push_back(flow);

        for(int ei = prevEdge[graph->targetIdx]; ei != -1; ei = prevEdge[graph->edgeVec[ei].from])
        {
            graph->edgeVec[ei].updatedCapacity -= flow;
            graph->edgeVec[ei^1].updatedCapacity += flow;

            costSum += flow * graph->edgeVec[ei].cost;
        }
    }

    costSum = (allFlow == graph->flowSum) ? costSum : INFI;

    return costSum;
}

int MinCostFlow::getFinalCost()
{
    reset();

    allAugmentedPaths.clear();
    allFlows.clear();

    while(findAugmentingPath(graph->sourceIdx, graph->targetIdx))
    {
        flow = INFI;
        std::vector<int> onePath;

        for(int ei = prevEdge[graph->targetIdx]; ei != -1; ei = prevEdge[graph->edgeVec[ei].from])
        {
            flow = flow > graph->edgeVec[ei].updatedCapacity ? graph->edgeVec[ei].updatedCapacity : flow;

            onePath.push_back(ei);
        }

        //save the current augmented path
        allAugmentedPaths.push_back(onePath);
        allFlows.push_back(flow);

        for(int ei = prevEdge[graph->targetIdx]; ei != -1; ei = prevEdge[graph->edgeVec[ei].from])
        {
            graph->edgeVec[ei].updatedCapacity -= flow;
            graph->edgeVec[ei^1].updatedCapacity += flow;

            costSum += flow * graph->edgeVec[ei].cost;

        }
    }

//    rankPath();

    combineUpAndDownFlow();

//    for( size_t i = 0; i < allAugmentedPaths.size(); i++ )
//    {
//       	if(!checkPathContinue(i))
//        {
//             outputPath(i);
//        }
//    }

//    for( size_t i = 0; i < allAugmentedPaths.size(); i++ )
//    {
//        if(allFlows[i] < 0)
//        {
//            std::cout<<"flow error: "<<std::endl;
//            outputPath(i);
//        }
//
//        if(allAugmentedPaths[i].size() < 3)
//        {
//            std::cout<<"path size < 3"<<std::endl;
//            outputPath(i);
//        }
//
//        if(graph->nodeTypes[ graph->edgeVec[allAugmentedPaths[i].front()].to ] != 4)
//        {
//            std::cout<<"end path error"<<std::endl;
//            outputPath(i);
//        }
//
//        if(graph->nodeTypes[ graph->edgeVec[allAugmentedPaths[i].back()].from ] != 3)
//        {
//            std::cout<<"start path error"<<std::endl;
//            outputPath(i);
//        }
//    }
//
    costSum = 0;
//    int flowSum = 0;
//    for( size_t i = 0; i < allAugmentedPaths.size(); i++ )
//    {
//        flowSum += allFlows[i];
//        for(size_t j = 1; j < allAugmentedPaths[i].size() - 1; j++)
//        {
//            costSum += graph->edgeVec[allAugmentedPaths[i][j]].cost * allFlows[i];
//        }
//    }
//    std::cout<<"costSum: "<<costSum<<std::endl;
//    std::cout<<"flowSum: "<<flowSum<<"("<<graph->flowSum<<")"<<std::endl;

//    costSum = (flowSum == graph->flowSum) ? costSum : INFI;

    return costSum;
}

void MinCostFlow::rankPath()
{
    size_t curPathIdx = 0;
    size_t revPathIdx = 0;
    size_t revPosIdx = 0;
    int revEdge = 0;
    bool flag = false;
    int minFlow = 0;
    std::vector<int> newPath;

    std::vector<int> curPath;
    std::vector<int> reversePath;

    while(curPathIdx < allAugmentedPaths.size())
    {
//        std::cout<<std::endl;
//        std::cout<<"id: "<<curPathIdx<<"("<<allAugmentedPaths.size()<<")"<<std::endl;

        if(0 == allFlows[curPathIdx])
        {
            curPathIdx ++;
            continue;
        }

        curPath = allAugmentedPaths[curPathIdx];
        flag = false;
        for(size_t i = 0; i < curPath.size(); i++)
        {
            //break;
            if(graph->edgeVec[curPath[i]].cost < 0)
            {
                revEdge = curPath[i]^1;
                findSpecificPath(revEdge, revPathIdx, revPosIdx);

//                outputPath(curPathIdx);
//                outputPath(revPathIdx);

//                std::cout<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//                std::cout<<"Flow: "<<allFlows[curPathIdx]<<" curPathIdx: "<<curPathIdx;
//                    <<" curPosIdx: "<<i<<" Edge: "<<curPath[i]<<std::endl;
//                    std::cout<<"Flow: "<<allFlows[revPathIdx]<<" revPathIdx: "<<revPathIdx
//                    <<" revPosIdx: "<<revPosIdx<<" Edge: "<<revEdge<<" "<<graph->edgeVec[revEdge].from<<" -> "<<graph->edgeVec[revEdge].to<<std::endl;

                //current edge and its reverse edge is on the same path
                if(curPathIdx == revPathIdx)
                {
                    minFlow = allFlows[curPathIdx];
                    allFlows[curPathIdx] -= minFlow;

                    newPath.clear();
                    size_t frontEdge = i > revPosIdx ? i : revPosIdx;
                    size_t backEdge = i < revPosIdx ? i : revPosIdx;
                    for(size_t j = 0; j < backEdge; j++)
                    {
                        newPath.push_back(curPath[j]);
                    }
                    for(size_t j = frontEdge + 1; j < curPath.size(); j++)
                    {
                        newPath.push_back(curPath[j]);
                    }

                    allAugmentedPaths.push_back(newPath);
                    allFlows.push_back(minFlow);

//                    if(!checkPathContinue(allAugmentedPaths.size() - 1))
//                    {
//                         outputPath(curPathIdx);
//                         std::cout<<"goal: "<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//
//                         outputPath(revPathIdx);
//                         outputPath(allAugmentedPaths.size() - 1);
//                    }
//                    outputPath(allAugmentedPaths.size() - 1);
                }
                else
                {
                    reversePath = allAugmentedPaths[revPathIdx];

                    minFlow = (allFlows[curPathIdx] < allFlows[revPathIdx]) ? allFlows[curPathIdx] : allFlows[revPathIdx];

                    //weaken two origin paths
                    allFlows[curPathIdx] = allFlows[curPathIdx] - minFlow;
                    allFlows[revPathIdx] = allFlows[revPathIdx] - minFlow;
//                    std::cout<<"minFlow: "<<minFlow<<std::endl;
//                    std::cout<<"allFlows[curPathIdx]: "<<allFlows[curPathIdx]<<std::endl;
//                    std::cout<<"allFlows[revPathIdx]: "<<allFlows[revPathIdx]<<std::endl;

                    //add two new paths
                    newPath.clear();
                    for(size_t j = 0; j < i; j++)
                    {
                        newPath.push_back(curPath[j]);
                    }
                    for(size_t j = revPosIdx + 1; j < reversePath.size(); j++)
                    {
                        newPath.push_back(reversePath[j]);
                    }
                    allAugmentedPaths.push_back(newPath);
                    allFlows.push_back(minFlow);

                    newPath.clear();
                    for(size_t j = 0; j < revPosIdx; j++)
                    {
                        newPath.push_back(reversePath[j]);
                    }
                    for(size_t j = i + 1; j < curPath.size(); j++)
                    {
                        newPath.push_back(curPath[j]);
                    }
                    allAugmentedPaths.push_back(newPath);
                    allFlows.push_back(minFlow);

//                    if(!checkPathContinue(allAugmentedPaths.size() - 2))
//                    {
//                         outputPath(curPathIdx);
//                         std::cout<<"goal: "<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//
//                         outputPath(revPathIdx);
//                         outputPath(allAugmentedPaths.size() - 2);
//                    }
//
//                    if(!checkPathContinue(allAugmentedPaths.size() - 1))
//                    {
//                         outputPath(curPathIdx);
//                         std::cout<<"goal: "<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//
//                         outputPath(revPathIdx);
//                         outputPath(allAugmentedPaths.size() - 1);
//                    }

//                    outputPath(allAugmentedPaths.size() - 2);
//                    outputPath(allAugmentedPaths.size() - 1);
                }

                flag = true;

            }

            if(flag)
            {
                break;
            }
        }

        if(!flag)
        {
            curPathIdx ++;
        }
    }
}

void MinCostFlow::findSpecificPath(int edge, size_t& pathIdx, size_t& posIdx)
{
    bool flag = false;
    for( size_t i = 0; i < allAugmentedPaths.size(); i++ )
    {
        if( 0 == allFlows[i] )
        {
            continue;
        }

        for( size_t j = 0; j < allAugmentedPaths[i].size(); j++ )
        {
            if(edge == allAugmentedPaths[i][j])
            {
                pathIdx = i;
                posIdx = j;
                flag = true;
                break;
            }
        }

        if(flag)
        {
            break;
        }
    }
}

void MinCostFlow::saveResultToFile(char* filename)
{
    std::vector<char> outputStr;
    char buff[10];

    std::cout<<std::endl;
    int allFlow = 0;
    std::vector<int> validPathVec;

    for(size_t i = 0; i < allAugmentedPaths.size(); i++)
    {
        if(0 == allFlows[i])
        {
            continue;
        }

        validPathVec.push_back(i);
    }

    int pathNum = validPathVec.size();
    putIntToCharVector(outputStr, pathNum, buff);
    outputStr.push_back('\n');
    outputStr.push_back('\n');

    int nodeID = 0;
    for(size_t i = 0; i < validPathVec.size(); i++)
    {
        allFlow += allFlows[validPathVec[i]];
//        outputPath(validPathVec[i]);
        for(int j = (int)allAugmentedPaths[validPathVec[i]].size() - 2; j > 0; j--)
        {
            nodeID = graph->edgeVec[allAugmentedPaths[validPathVec[i]][j]].from;

//            if(nodeID < 0 || nodeID>= graph->commonNodeNum)
//            {
//                std::cout<<"common error"<<std::endl;
//            }

            //std::cout<<nodeID<<" -> ";
            putIntToCharVector(outputStr, nodeID, buff);
            outputStr.push_back(' ');
        }
        //j = 1
        nodeID = graph->edgeVec[allAugmentedPaths[validPathVec[i]][0]].from - graph->commonNodeNum;

//        if(nodeID < 0 || nodeID>= graph->costNodeNum)
//        {
//            std::cout<<"cost error"<<std::endl;
//
//        }

        putIntToCharVector(outputStr, nodeID, buff);
        outputStr.push_back(' ');
        putIntToCharVector(outputStr, allFlows[validPathVec[i]], buff);
        outputStr.push_back('\n');
        //std::cout<<nodeID<<" "<<allFlows[validPathVec[i]];

//        std::cout<<std::endl;
    }

    outputStr.push_back('\0');
//    std::cout<<"allFlow: "<<allFlow<<"("<<graph->flowSum<<")"<<std::endl;
//    std::cout<<"outputStr: "<<std::endl<<outputStr.data()<<std::endl;

	//char * topo_file = (char *)"17\n\n0 8 0 20\n21 8 0 20\n9 11 1 13\n21 22 2 20\n23 22 2 8\n1 3 3 11\n24 3 3 17\n27 3 3 26\n24 3 3 10\n18 17 4 11\n1 19 5 26\n1 16 6 15\n15 13 7 13\n4 5 8 18\n2 25 9 15\n0 7 10 10\n23 24 11 23";
	write_result(outputStr.data(), filename);

}

bool MinCostFlow::findAugmentingPath(int s, int t)
{
    int currNodeIdx;
    for(int i = 0; i < nodeNum; i++)
    {
        costDist[i] = INFI;
        nodeVis[i] = 0;
        prevEdge[i] = -1;
    }

    std::queue<int> q;
    q.push(s);
    costDist[s] = 0;
    nodeVis[s] = 1;
    int to = 0;

    while(!q.empty())
    {
        currNodeIdx = q.front();
        q.pop();
        nodeVis[currNodeIdx] = 0;

        for(int ei = graph->nodeVec[currNodeIdx].edgeHead; ei != -1; ei = graph->edgeVec[ei].next)
        {
            if(graph->edgeVec[ei].updatedCapacity)
            {
                to = graph->edgeVec[ei].to;
                if(costDist[to] > costDist[currNodeIdx] + graph->edgeVec[ei].cost)
                {
                    costDist[to] = costDist[currNodeIdx] + graph->edgeVec[ei].cost;
                    prevEdge[to] = ei;

                    if(!nodeVis[to])
                    {
                        nodeVis[to] = 1;
                        q.push(to);
                    }
                }
            }
        }
    }

    return costDist[t] != INFI;
}

void MinCostFlow::reset()
{
    graph->restoreEdgeCapacity();

//getCost valuables
    costSum = 0;
    flow = INFI;
    allFlow = 0;
}

void MinCostFlow::outputPath(int pathID)
{
    std::cout<<"id: "<<pathID<<" : ";
    for(size_t j = 0; j < allAugmentedPaths[pathID].size(); j++)
    {
        std::cout<<graph->edgeVec[allAugmentedPaths[pathID][j]].to<<" <- "
        <<graph->edgeVec[allAugmentedPaths[pathID][j]].from <<",";
    }
    std::cout<<"("<<allFlows[pathID]<<")"<<std::endl;
}

void MinCostFlow::putIntToCharVector(std::vector<char>& outputStr, int& intNum, char* buf)
{
    sprintf(buf, "%d%c", intNum, '\0');
    char* head = buf;
    while(*head != '\0')
    {
        outputStr.push_back(*head);
        head++;
    }
}

void MinCostFlow::combineUpAndDownFlow()
{
//    std::cout<<"allAugmentedPaths.size(): "<<allAugmentedPaths.size()<<std::endl;
//    std::cout<<"allFlows.size(): "<<allAugmentedPaths.size()<<std::endl;
    for(int i = (int)allAugmentedPaths.size() - 1; i >= 0 ; i--)
    {
        if(0 == allFlows[i])
        {
            allAugmentedPaths.erase(allAugmentedPaths.begin() + i);
            allFlows.erase(allFlows.begin() + i);
        }
    }
//    std::cout<<"delete flow=0 allAugmentedPaths.size(): "<<allAugmentedPaths.size()<<std::endl;
//    std::cout<<"delete flow=0 allFlows.size(): "<<allFlows.size()<<std::endl;

    size_t curPathIdx = 0;
    size_t revPathIdx = 0;
    size_t revPosIdx = 0;
    int revEdge = 0;
    bool flag = false;
    int minFlow = 0;
    std::vector<int> newPath;

    std::vector<int> curPath;
    std::vector<int> reversePath;

    while(curPathIdx < allAugmentedPaths.size())
    {
//        std::cout<<std::endl;
//        std::cout<<"id: "<<curPathIdx<<"("<<allAugmentedPaths.size()<<")"<<std::endl;

        if(0 == allFlows[curPathIdx])
        {
            curPathIdx ++;
            continue;
        }

        curPath = allAugmentedPaths[curPathIdx];
        flag = false;
        for(size_t i = 0; i < curPath.size(); i++)
        {
            revEdge = findDownFlow(graph->edgeVec[curPath[i]].to, graph->edgeVec[curPath[i]].from, revPathIdx, revPosIdx);

            if(revEdge == -1)
            {
                continue;
            }

//            outputPath(curPathIdx);
//            outputPath(revPathIdx);

//                std::cout<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//                std::cout<<"Flow: "<<allFlows[curPathIdx]<<" curPathIdx: "<<curPathIdx;
//                    <<" curPosIdx: "<<i<<" Edge: "<<curPath[i]<<std::endl;
//                    std::cout<<"Flow: "<<allFlows[revPathIdx]<<" revPathIdx: "<<revPathIdx
//                    <<" revPosIdx: "<<revPosIdx<<" Edge: "<<revEdge<<" "<<graph->edgeVec[revEdge].from<<" -> "<<graph->edgeVec[revEdge].to<<std::endl;

                //current edge and its reverse edge is on the same path
            if(curPathIdx == revPathIdx)
            {
                minFlow = allFlows[curPathIdx];
                allFlows[curPathIdx] -= minFlow;

                newPath.clear();
                size_t frontEdge = i > revPosIdx ? i : revPosIdx;
                size_t backEdge = i < revPosIdx ? i : revPosIdx;
                for(size_t j = 0; j < backEdge; j++)
                {
                    newPath.push_back(curPath[j]);
                }
                for(size_t j = frontEdge + 1; j < curPath.size(); j++)
                {
                    newPath.push_back(curPath[j]);
                }
                allAugmentedPaths.push_back(newPath);
                allFlows.push_back(minFlow);

//                if(!checkPathContinue(allAugmentedPaths.size() - 1))
//                {
//                    outputPath(curPathIdx);
//                    std::cout<<"goal: "<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//
//                    outputPath(revPathIdx);
//                    outputPath(allAugmentedPaths.size() - 1);
//                }
//                    outputPath(allAugmentedPaths.size() - 1);
            }
            else
            {
                reversePath = allAugmentedPaths[revPathIdx];

                minFlow = (allFlows[curPathIdx] < allFlows[revPathIdx]) ? allFlows[curPathIdx] : allFlows[revPathIdx];

                    //weaken two origin paths
                allFlows[curPathIdx] = allFlows[curPathIdx] - minFlow;
                allFlows[revPathIdx] = allFlows[revPathIdx] - minFlow;
//                    std::cout<<"minFlow: "<<minFlow<<std::endl;
//                    std::cout<<"allFlows[curPathIdx]: "<<allFlows[curPathIdx]<<std::endl;
//                    std::cout<<"allFlows[revPathIdx]: "<<allFlows[revPathIdx]<<std::endl;

                    //add two new paths
                newPath.clear();
                for(size_t j = 0; j < i; j++)
                {
                    newPath.push_back(curPath[j]);
                }
                for(size_t j = revPosIdx + 1; j < reversePath.size(); j++)
                {
                    newPath.push_back(reversePath[j]);
                }
                allAugmentedPaths.push_back(newPath);
                allFlows.push_back(minFlow);

                newPath.clear();
                for(size_t j = 0; j < revPosIdx; j++)
                {
                    newPath.push_back(reversePath[j]);
                }
                for(size_t j = i + 1; j < curPath.size(); j++)
                {
                    newPath.push_back(curPath[j]);
                }
                allAugmentedPaths.push_back(newPath);
                allFlows.push_back(minFlow);

//                if(!checkPathContinue(allAugmentedPaths.size() - 2))
//                {
//                    outputPath(curPathIdx);
//                    std::cout<<"goal: "<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//
//                    outputPath(revPathIdx);
//                    outputPath(allAugmentedPaths.size() - 2);
//                }
//
//                if(!checkPathContinue(allAugmentedPaths.size() - 1))
//                {
//                    outputPath(curPathIdx);
//                    std::cout<<"goal: "<<graph->edgeVec[curPath[i]].to<<" <- "<<graph->edgeVec[curPath[i]].from<<std::endl;
//
//                    outputPath(revPathIdx);
//                    outputPath(allAugmentedPaths.size() - 1);
//                }
//                    outputPath(allAugmentedPaths.size() - 2);
//                    outputPath(allAugmentedPaths.size() - 1);
            }

            flag = true;
            break;
        }

        if(!flag)
        {
            curPathIdx ++;
        }
    }
}

int MinCostFlow::findDownFlow(int from, int to, size_t& pathIdx, size_t&posIdx)
{
    bool flag = false;
    int edge = -1;
    for( size_t i = 0; i < allAugmentedPaths.size(); i++ )
    {
        if( 0 == allFlows[i] )
        {
            continue;
        }

        for( size_t j = 0; j < allAugmentedPaths[i].size(); j++ )
        {
            if(graph->edgeVec[allAugmentedPaths[i][j]].from == from
               && graph->edgeVec[allAugmentedPaths[i][j]].to == to)
            {
                pathIdx = i;
                posIdx = j;
                edge = allAugmentedPaths[i][j];
                flag = true;
                break;
            }
        }

        if(flag)
        {
            break;
        }
    }

    return edge;
}

bool MinCostFlow::checkPathContinue(int pathID)
{
    bool flag = true;
    for( int i = 0; i < (int)allAugmentedPaths[pathID].size() - 1; i++ )
    {
         if(graph->edgeVec[allAugmentedPaths[pathID][i]].from != graph->edgeVec[allAugmentedPaths[pathID][i + 1]].to)
         {
              flag = false;
              break;
         }
    }
    return flag;
}
